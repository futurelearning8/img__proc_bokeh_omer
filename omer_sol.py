import cv2
import numpy as np
import pandas as pd
import math


class bokeh():
    #TODO - transform to class in order to relate the current frame via self
    # gaussian filter on the chosen ...
    def __init__(self):
        self.TH = [180,120,120]
        self.N = 5
        self.blur_bool = False
        self.blur_segment_num = False
        self.current_frame = None

    def initialization(self,input_video_path ):
        n=self.N
        cap = cv2.VideoCapture(input_video_path,)
        ret, frame = cap.read()

        height, width, channels = frame.shape
        frames_to_median = np.zeros(shape=(n,height,width,3))
        frames_to_median[0, :, :, :] = frame


        for i in range (1,n-1):
            ret, frame = cap.read()
            frames_to_median[i,:,:,:] = frame

        median_frame = np.median(frames_to_median,axis=0)
        return median_frame

    def calc_diff_frame(mean_frame,curr_frame):
        return mean_frame - curr_frame

    def running(median_frame, curr_frame, threshold = TH):
        height, width, channels = curr_frame.shape
        curr_frame_hsv = cv2.cvtColor(curr_frame, cv2.COLOR_RGB2HSV)
        median_frame = median_frame.astype("float32")
        median_frame_hsv = cv2.cvtColor(median_frame, cv2.COLOR_RGB2HSV)
        diff = calc_diff_frame(median_frame_hsv , curr_frame_hsv)
        mask = np.zeros(shape=(height, width))

        mask = np.sqrt(diff[:,:,0]**2 + diff[:,:,1]**2)
        mask[mask < threshold] = 0
        mask[mask >= threshold] = 1

        return mask

    def left_click_event(event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            num_labels, labels , stats, centroids= current_frame.connectedComponentsWithStats(TH[0], 4, cv2.CV_32S)
            if labels[y,x] :
                blur_bool = True
            else:
                blur_all = True

def main():
    paths = ["data/car_with_balloons.mp4","data/dorian.mp4","data/woman_on_floor.mp4"]
    th_index = 0
    for path in paths:
        vidcap = cv2.VideoCapture(path)

        # Initialization
        median_frame = initialization(path)
        #running
        success = True
        while success:
            success, curr_frame = vidcap.read()
            if success == False:
                break
            frame_bokeh = running(median_frame, curr_frame,threshold=TH[th_index])
            mask =  frame_bokeh.astype("uint8")  # mask is only
            image =  cv2.bitwise_and(curr_frame,curr_frame,mask = mask)
            cv2.imshow("bokeh", image)
            current_frame = curr_frame
            output = cv2.setMouseCallback("bokeh", left_click_event)
            if blur_bool:
                print("ping")
            elif blur_segment_num:
                print("pong")
            cv2.waitKey(30)
        th_index+=1

if __name__ == '__main__':
    main()
